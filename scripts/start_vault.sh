#!/usr/bin/env bash

# NOTE: For Keys , see ../vault_data/vault-cluster.json 
# e.g. for unsealing the server

K2SO_BASE="$HOME/k2so"

docker run --name vault --cap-add=IPC_LOCK -d  -p 8200:8200 \
  -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' \
  vault 
