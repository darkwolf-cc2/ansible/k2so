#!/usr/bin/env bash

# Build the Backend Services using the services.yml compose config

docker-compose -f services.yml up --build -d 
