

-- Table: public.operator

-- DROP TABLE public.operator;

CREATE TABLE public.operator
(
    id integer NOT NULL,
    name character varying(100) COLLATE pg_catalog."default",
    value integer,
    CONSTRAINT operator_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.operator
    OWNER to root;


insert into operator values (1,'greg',100);
insert into operator values (2,'lucy',200);

select * from operator;
